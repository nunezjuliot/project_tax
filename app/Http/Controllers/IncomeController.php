<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Coin;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IncomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('calculateInventory')
             ->only('update', 'store', 'destroy', 'destroyAll');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->input('count', '5');

        $incomes = Transaction::whereIn('action',
                                [
                                  'INCOME',
                                  'MINING',
                                  'GIFT/TIP'
                                ]
                              )
                              ->orderby('date')
                              ->paginate($count);
        return $incomes;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $income = new Transaction();

        $income->exchange = $request->input('source');
        $income->currency = $request->input('currency');
        $income->symbol = $request->input('coin');
        $income->action = $request->input('type');
        $income->volumen = $request->input('volumen');
        $income->price = $request->input('price');
        $income->type = 'IN';
        $income->date = new carbon(
          $request->input('date').$request->input('time')
        );

        $income->taxyear = $income->date->year;

        if ($request->has('memo')) {
            $income->memo = $request->input('memo');
        }

        $income->total = $income->price;

        $income->save();

        activity()
          ->performedOn($income)
          ->causedBy($request->user())
          ->log('Registro income.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $income
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $income)
    {
        $income = Transaction::where('id', $income)
                  ->whereIn('action', ['INCOME','MINING','GIFT/TIP'])
                  ->firstOrFail();

        activity()
          ->performedOn($income)
          ->causedBy($request->user())
          ->log('Consulto income.');

        return $income;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Income  $income
     * @return \Illuminate\Http\Response
     */
    public function edit(Income $income)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Int $income
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $income)
    {
        $income = Transaction::where('id', $income)
                  ->whereIn('action', ['INCOME','MINING','GIFT/TIP'])
                  ->firstOrFail();

        $income->currency = $request->input('currency');
        $income->exchange = $request->input('exchange');
        $income->symbol = $request->input('symbol');
        $income->action = $request->input('action');
        $income->volumen = $request->input('volumen');
        $income->price = $request->input('price');

        //Change date
        $income->date = new Carbon(
          $request->input('datef').$income->date->toTimeString()
        );

        //Change time
        $income->date = new Carbon(
          $income->date->toDateString().$request->input('timef')
        );

        $income->memo = $request->input('memo');
        $income->taxyear = $income->date->year;
        $income->total = $income->price;

        $income->save();

        activity()
          ->performedOn($income)
          ->causedBy($request->user())
          ->log('Edito income.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Int $income
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $income)
    {
        $income = Transaction::where('id', $income)
                  ->whereIn('action', ['INCOME','MINING','GIFT/TIP'])
                  ->firstOrFail();

        $income->delete();

        activity()
          ->performedOn($income)
          ->causedBy($request->user())
          ->log('Elimino income.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Remove all resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyAll()
    {
        Transaction::whereIn('action',
                    [
                      'INCOME',
                      'MINING',
                      'GIFT/TIP'
                    ])
                    ->delete();

        return response()->json([
          'saved' => true
        ]);
    }
}
