<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class AdminController extends Controller
{
    /**
     * Display panel of administration.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Activity::orderby('id', 'desc')->simplePaginate(5);

        return view('admin.index', compact('events'));
    }
}
