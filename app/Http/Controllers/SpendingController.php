<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Coin;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SpendingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('calculateInventory')
             ->only('update', 'store', 'destroy', 'destroyAll');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->input('count', '5');

        $spendings = Transaction::whereIn('action',
                              [
                                'SPEND',
                                'STOLEN',
                                'GIFT',
                                'DONATION',
                                'LOST'
                               ]
                              )
                              ->orderby('date')
                              ->paginate($count);
        return $spendings;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $spending = new Transaction();

        $spending->exchange = $request->input('source');
        $spending->currency = $request->input('currency');
        $spending->symbol = $request->input('coin');
        $spending->action = $request->input('type');
        $spending->volumen = $request->input('volumen');
        $spending->price = $request->input('price');
        $spending->type = 'OUT';
        $spending->date = new carbon(
          $request->input('date').$request->input('time')
        );

        $spending->taxyear = $spending->date->year;

        if ($request->has('memo')) {
            $spending->memo = $request->input('memo');
        }

        $spending->total = $spending->price;

        $spending->save();

        activity()
          ->performedOn($spending)
          ->causedBy($request->user())
          ->log('Registo spending.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Int $spending
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $spending)
    {
        $spending = Transaction::where('id', $spending)
                    ->whereIn('action',
                      [
                        'SPEND',
                        'STOLEN',
                        'GIFT',
                        'DONATION',
                        'LOST'
                       ]
                     )
                     ->firstOrFail();

        activity()
          ->performedOn($spending)
          ->causedBy($request->user())
          ->log('Consulto spending.');

        return $spending;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Spending  $spending
     * @return \Illuminate\Http\Response
     */
    public function edit(Spending $spending)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Int $request
     * @param  \App\Models\Spending  $spending
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $spending)
    {
        $spending = Transaction::where('id', $spending)
                    ->whereIn('action',
                      [
                        'SPEND',
                        'STOLEN',
                        'GIFT',
                        'DONATION',
                        'LOST'
                       ]
                     )
                     ->firstOrFail();

        $spending->currency  = $request->input('currency');
        $spending->exchange  = $request->input('exchange');
        $spending->symbol  = $request->input('symbol');
        $spending->action  = $request->input('action');
        $spending->volumen = $request->input('volumen');
        $spending->price   = $request->input('price');

        //Change date
        $spending->date = new Carbon(
          $request->input('datef').$spending->date->toTimeString()
        );

        //Change time
        $spending->date = new Carbon(
          $spending->date->toDateString().$request->input('timef')
        );

        $spending->taxyear = $spending->date->year;
        $spending->memo = $request->input('memo');

        $spending->total = $spending->price;

        $spending->save();

        activity()
          ->performedOn($spending)
          ->causedBy($request->user())
          ->log('Edito spending.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Int $spending
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $spending)
    {
        $spending = Transaction::where('id', $spending)
                    ->whereIn('action',
                      [
                        'SPEND',
                        'STOLEN',
                        'GIFT',
                        'DONATION',
                        'LOST'
                       ]
                     )
                     ->firstOrFail();

        $spending->delete();

        activity()
          ->performedOn($spending)
          ->causedBy($request->user())
          ->log('Elimino spending.');

        return response()->json([
           'saved' => true
        ]);
    }

    /**
     * Remove all resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyAll()
    {
        Transaction::whereIn('action',
                    [
                      'SPEND',
                      'STOLEN',
                      'GIFT',
                      'DONATION',
                      'LOST'
                    ])
                    ->delete();

        return response()->json([
          'saved' => true
        ]);
    }
}
