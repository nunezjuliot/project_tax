<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Coin;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OpeningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('calculateInventory')
             ->only('update', 'store', 'destroy', 'destroyAll');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->input('count', '5');

        $openings = Transaction::whereIn('action', ['OPENING'])
                    ->orderby('date')
                    ->paginate($count);

        return $openings;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $opening = new Transaction();

        $opening->exchange = $request->input('source');
        $opening->action   = 'OPENING';
        $opening->currency = $request->input('currency');
        $opening->symbol   = $request->input('coin');
        $opening->volumen  = $request->input('volumen');
        $opening->price    = $request->input('price');
        $opening->fee = $request->input('fee', 0);
        $opening->type = 'IN';

        $opening->date = new carbon(
          $request->input('date').$request->input('time')
        );

        $opening->taxyear = $opening->date->year;

        if ($request->has('memo')) {
            $opening->memo = $request->input('memo');
        }

        $opening->total =
            ($opening->volumen * $opening->price) + $opening->fee;

        $opening->save();

        activity()
          ->performedOn($opening)
          ->causedBy($request->user())
          ->log('Registro opening.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Int $opening
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $opening)
    {
        $opening = Transaction::where('id', $opening)
                   ->whereIn('action', ['OPENING'])
                   ->firstOrFail();

        activity()
          ->performedOn($opening)
          ->causedBy($request->user())
          ->log('Consulto opening.');

        return $opening;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Opening  $opening
     * @return \Illuminate\Http\Response
     */
    public function edit(Opening $opening)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Int $opening
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $opening)
    {
        $opening = Transaction::where('id', $opening)
                   ->whereIn('action', ['OPENING'])
                    ->firstOrFail();

        $opening->currency = $request->input('currency');
        $opening->exchange = $request->input('exchange');
        $opening->symbol = $request->input('symbol');
        $opening->volumen = $request->input('volumen');
        $opening->price = $request->input('price');
        $opening->memo = $request->input('memo');

        //Change date
        $opening->date = new Carbon(
          $request->input('datef').$opening->date->toTimeString()
        );

        //Change time
        $opening->date = new Carbon(
          $opening->date->toDateString().$request->input('timef')
        );

        $opening->fee = $request->input('fee', 0);
        $opening->taxyear = $opening->date->year;

        $opening->total =
            ($opening->volumen * $opening->price) + $opening->fee;

        $opening->save();

        activity()
          ->performedOn($opening)
          ->causedBy($request->user())
          ->log('Edito opening.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Int  $opening
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $opening)
    {
        $opening = Transaction::where('id', $opening)
                   ->whereIn('action', ['OPENING'])
                   ->firstOrFail();

        $opening->delete();

        activity()
          ->performedOn($opening)
          ->causedBy($request->user())
          ->log('Elimino opening.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Remove all resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyAll()
    {
        Transaction::where('action', 'OPENING')
                    ->delete();

        return response()->json([
          'saved' => true
        ]);
    }
}
