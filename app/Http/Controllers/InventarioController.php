<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inventory_operation;
use App\Models\Inventory_coin;
use DB;

class InventarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function gains(Request $request)
    {
        $count = $request->input('count', '5');

        $columnstoSelect = [
          'inventory_operations.output',
          DB::raw('ROUND(sum(inventory_operations.volume), 8) as volume'),
          DB::raw('SUM(inventory_operations.cost_basis) /
                   SUM(inventory_operations.volume)
                   as acquired_cost'),
          DB::raw('any_value(inventory_operations.sold_cost)
                  as sold_cost'),
          DB::raw('any_value(DATE_FORMAT(transactions.date,"%d/%m/%Y"))
                  as date'),
          DB::raw('"Some transactions not resolved" as message'),
          DB::raw('sum(inventory_operations.cost_basis) as cost_basis'),
          DB::raw('sum(inventory_operations.procceds) as procceds'),
          DB::raw('sum(inventory_operations.gain_lost) as gain_lost'),
          DB::raw('min(inventory_operations.resolved) as resolved'),
          DB::raw('any_value(transactions.symbol) as symbol'),
          DB::raw('any_value(transactions.currency) as currency'),
          DB::raw('min(inventory_operations.stock) as stock')
        ];

        $operations =  inventory_operation::
                       join('transactions',
                            'transactions.id',
                            '=',
                            'inventory_operations.output'
                        )
                        ->select($columnstoSelect)
                        ->groupBy('inventory_operations.output')
                        ->orderBy('transactions.date')
                        ->paginate($count);

        return $operations;
    }

    public function coins(Request $request)
    {
        $count = $request->input('count', '5');
        return Inventory_coin::paginate($count);
    }

    public function coinsTotal()
    {
        $coins = Inventory_coin::groupBy('coin', 'currency')
                ->select(
                  'coin',
                  'currency',
                  DB::raw('ROUND(SUM(volumen),8) as volume'),
                  DB::raw('SUM(cost_basis) as cost_basis'),
                  DB::raw('SUM(cost_basis) / SUM(volumen) as weighted_price')
                )
                ->get();

        $total = Inventory_coin::select(DB::raw('SUM(cost_basis) as cost_basis'))
               ->first();

        return response()->json([
          'coins' => $coins,
          'total' => $total
      ]);
    }

    public function sellDetail(Request $request, $output)
    {
        $count = $request->input('count', '5');

        return inventory_operation::with('input', 'output')
               ->where('output', $output)->paginate($count);
    }
}
