<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TradingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('calculateInventory')
             ->only('update', 'store', 'destroy', 'destroyAll');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->input('count', '5');

        $tradings = Transaction::whereIn('action', ['BUY','SELL'])
                              ->orderby('date')
                              ->paginate($count);

        return $tradings;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $trading = new Transaction();

        $trading->exchange = $request->input('exchange');
        $trading->symbol = $request->input('coin');
        $trading->currency = $request->input('currency');
        $trading->action = $request->input('type');
        $trading->volumen = $request->input('volumen');

        if ($request->has('price')) {
            $trading->price = $request->input('price');
        } else {
            $trading->price = (float)
            $request->input('cost') / $request->input('volumen');
        }

        $trading->date = new carbon(
          $request->input('date').$request->input('time')
        );

        $trading->fee = $request->input('fee', 0);

        if ($request->has('memo')) {
            $trading->memo = $request->input('memo');
        }

        $trading->taxyear = $trading->date->year;

        //Calculate total value of the transaction
        switch ($trading->action) {
          case 'BUY':
              $value = -(($trading->volumen * $trading->price) + $trading->fee);
              $trading->type = 'IN';
            break;

          case 'SELL':
              $value = (($trading->volumen * $trading->price) - $trading->fee);
              $trading->type = 'OUT';
            break;

          default:
              abort(403, 'Unauthorized action.');
            break;
        }

        $trading->total = $value;
        $trading->save();

        activity()
          ->performedOn($trading)
          ->causedBy($request->user())
          ->log('Registro trading.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Int  $trading
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $trading)
    {
        $trading = Transaction::where('id', $trading)
                 ->whereIn('action', ['BUY','SELL'])
                 ->firstOrFail();

        activity()
        ->performedOn($trading)
        ->causedBy($request->user())
        ->log('Consulto trading.');

        return $trading;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trading  $trading
     * @return \Illuminate\Http\Response
     */
    public function edit(Trading $trading)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Int $request
     * @param  \App\Models\Trading  $trading
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $trading)
    {
        $trading = Transaction::where('id', $trading)
                   ->whereIn('action', ['BUY','SELL'])
                   ->firstOrFail();

        $trading->currency = $request->input('currency');
        $trading->exchange = $request->input('exchange');
        $trading->symbol = $request->input('symbol');
        $trading->action = $request->input('action');
        $trading->volumen = $request->input('volumen');
        $trading->fee = $request->input('fee');

        if ($request->has('price')) {
            $trading->price = $request->input('price');
        } elseif ($request->has('cost')) {
            $trading->price = (float)
            $request->input('cost') / $request->input('volumen');
        }

        //Change date
        $trading->date = new Carbon(
          $request->input('datef').$trading->date->toTimeString()
        );

        //Change time
        $trading->date = new Carbon(
          $trading->date->toDateString().$request->input('timef')
        );

        $trading->memo = $request->input('memo');

        //Calculate total value of the transaction
        switch ($trading->action) {
          case 'BUY':
              $value = -(($trading->volumen * $trading->price) + $trading->fee);
              $trading->type = 'IN';
            break;

          case 'SELL':
              $value = (($trading->volumen * $trading->price) - $trading->fee);
              $trading->type = 'OUT';
            break;

          default:
              abort(403, 'Unauthorized action.');
            break;
        }

        $trading->total = $value;
        $trading->save();

        activity()
          ->performedOn($trading)
          ->causedBy($request->user())
          ->log('Edito trading.');

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Int $trading
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $trading)
    {
        $trading = Transaction::where('id', $trading)
                 ->whereIn('action', ['BUY','SELL'])
                 ->firstOrFail();

        $trading->delete();

        activity()
        ->performedOn($trading)
        ->causedBy($request->user())
        ->log('Elimino trading.');

        return response()->json([
          'saved' => true
      ]);
    }

    /**
     * Remove all resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyAll()
    {
        Transaction::whereIn('action', ['BUY','SELL'])
                    ->delete();

        return response()->json([
          'saved' => true
        ]);
    }
}
