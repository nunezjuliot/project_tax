<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\Inventario\InventarioRepository;

class calculateInventory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $inventario = new InventarioRepository('Fifo');
        $inventario->calculate();
        $inventario->save();
    }
}
