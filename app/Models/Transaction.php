<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = [
       'created_at',
       'updated_at',
       'deleted_at',
       'date'
    ];

    /**
     * Attributes that will be hidden in formatting
     * @var array
     */
    protected $hidden = [
      'created_at',
      'updated_at'
    ];

    /**
     * Additional Attributes that will be appends in formatting
     * @var array
     */
    protected $appends = [
      'datef',
      'timef'
    ];

    /**
    * Formatting the volumen attribute
    */
    public function getVolumenAttribute()
    {
        return round($this->attributes['volumen'],8);
    }

    /**
    * Formatting the price attribute
    */
    public function getPriceAttribute()
    {
        return $this->attributes['price'];
    }

    /**
    * Formatting the fees attribute
    */
    public function getFeeAttribute()
    {
        return $this->attributes['fee'];
    }

    /**
    * Formatting the total attribute
    */
    public function getTotalAttribute()
    {
        return $this->attributes['total'];
    }

    /**
     * Set date attribute that contain the date portion of
     * made timestamp attributes.
     */
    public function getDatefAttribute()
    {
        return $this->date->toDateString();
    }

    /**
     * Set time attribute that contain the time portion of
     * made timestamp attributes.
     */
    public function getTimefAttribute()
    {
        return $this->date->toTimeString();
    }
}
