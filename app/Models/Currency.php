<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * Attributes that will be hidden in formatting
     */
    protected $hidden = ['created_at', 'updated_at'];
}
