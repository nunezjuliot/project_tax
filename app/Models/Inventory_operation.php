<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory_operation extends Model
{
    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = [
       'created_at',
       'updated_at',
       'deleted_at'
    ];

    /**
     * Attributes that will be hidden in formatting
     * @var array
     */
    protected $hidden = [
      'date_sold'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
    * Formatting the volumen attribute
    */
    public function getVolumeAttribute()
    {
        return round($this->attributes['volume'],8);
    }

    /**
    * Round the stock attribute
    */
    public function getStockAttribute()
    {
        return round($this->attributes['stock'],8);
    }

    /**
      * Get the input transction record associated with the operation.
      */
     public function input()
     {
         return $this->hasOne('App\Models\Transaction',
                              'id', 'input');
     }

     /**
       * Get the output transaction record associated with the operation.
       */
      public function output()
      {
          return $this->hasOne('App\Models\Transaction',
                               'id', 'output');
      }
}
