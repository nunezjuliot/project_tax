<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory_coin extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
    * Formatting the volumen attribute
    */
    public function getVolumenAttribute()
    {
        return round($this->attributes['volumen'],8);
    }
}
