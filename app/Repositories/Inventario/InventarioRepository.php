<?php

namespace App\Repositories\Inventario;

use App\Repositories\Inventario\Methods\Fifo;
use App\Repositories\Inventario\Methods\Lifo;
use App\Repositories\Inventario\Methods\Avg;
use App\Models\Inventory_operation;
use App\Models\Inventory_coin;

class InventarioRepository
{
    private $method;
    private $capitalGains;
    private $coins;

    public function __construct($method)
    {
        $this->method = $method;
    }

    public function getCapitalGains()
    {
        return $this->capitalGains;
    }

    public function getCoins()
    {
        return $this->coins;
    }

    public function calculate()
    {
        $method = new Avg();
        $method->calculate();
        $this->capitalGains = $method->getOperations();
        $this->coins = $method->getCoins();
    }

    public function save()
    {

      //Delete all previous calculations
      Inventory_operation::truncate();
      Inventory_coin::truncate();

      //Insert the new calculations
      Inventory_operation::insert($this->capitalGains);
      Inventory_coin::insert($this->coins);
    }
}
