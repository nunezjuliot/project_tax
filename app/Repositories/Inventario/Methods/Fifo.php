<?php

namespace App\Repositories\Inventario\Methods;

use App\Models\Transaction;
use App\Repositories\Inventario\InventoryMethod;

class Fifo implements InventoryMethod
{
    private $operations = array();
    private $coins = array();
    private $outputs;
    private $inputs;

    public function calculate()
    {
        $this->getTransactions();
        $this->inventarioCalculate($this->inputs, $this->outputs);
    }

    public function getOperations()
    {
        return $this->operations;
    }

    public function getCoins()
    {
        return $this->coins;
    }

    private function inventarioCalculate($inputs, $outputs)
    {

      //Calculate operations Sales
      foreach ($outputs as $output) {

          // Obtiene las entradas que pueden resolver la salida
          $inputsToResolve = $inputs->where('volumen', '>', 0)
                           ->where('date', '<=', $output->date)
                           ->where('symbol', '=', $output->symbol);

           if ($inputsToResolve->isEmpty()) {

               $operation = $this->calculateOperation($output, $output,
                                                      false, 0);
               // Almacena la operacion calculada.
               array_push($this->operations, $operation);
               continue;
           }

          foreach ($inputsToResolve as $input) {

              $balance = $input->volumen - $output->volumen;

              // Obtiene la cantidad total en existencia
              $stock = $inputsToResolve->sum('volumen');

              $operation = $this->calculateOperation($input, $output,
                                                     true, $stock);

              if ($balance >= 0) {
                  $input->volumen -= $output->volumen;
                  $output->volumen = 0;
              }
              if ($balance < 0) {
                  $output->volumen -= $input->volumen;
                  $input->volumen = 0;
              }

              array_push($this->operations, $operation);

              if ($output->volumen == 0) {
                  break;
              }
          }

          if ($output->volumen > 0) {
              $operation = $this->calculateOperation($output, $output,false, 0);
              array_push($this->operations, $operation);
          }
      }

        //Calculate coins in stock

        // Obtiene las entradas cuyo volumen sea mayor que cero.
        $inputs = $inputs->where('volumen', '>', 0);

        if (!$inputs->isEmpty()) {

            // Las entradas cuyo volumenes sean mayores que cero las almacena
            // como monedas que aun estan en el inventario.
            foreach ($inputs as $input) {
                $coin = array(
                'currency' => $input->currency,
                'date_acquired' => $input->date,
                'coin' => $input->symbol,
                'volumen' => $input->volumen,
                'price' => $input->price,
                'fee' => $input->fee,
                'cost_basis' => ($input->volumen * $input->price) + $input->fee
              );

                array_push($this->coins, $coin);
            }
        }
    }

    private function calculateOperation($input, $output,
        $resolved = true, $stock)
    {
        $balance = $input->volumen - $output->volumen;
        $message = '';

        if ($balance <= 0) {
            $volume = $input->volumen;
        } elseif ($balance > 0) {
            $volume = $output->volumen;
        }

        $procceds = ($volume * $output->price) -$output->fee;

        if ($resolved) {
            $cost_basis = ($volume * $input->price) -$input->fee;
        } else {
            $cost_basis = 0;
        }

        $gain_lost = $procceds - $cost_basis;
        $stock -= $volume;

        if (!$resolved) {
            $message =  sprintf("No matching buy for %f %s on %s "
                              ."Assuming zero-cost buy for %s %.2f on %s",
                                $output->volumen,
                                $output->symbol,
                                $output->datef,
                                $output->currency,
                                $gain_lost,
                                $output->datef
                              );
        }


        $operation = array(
          'input' => $input->id,
          'output' => $output->id,
          'volume' => $volume,
          'procceds' => $procceds,
          'cost_basis' => $cost_basis,
          'gain_lost' => $gain_lost,
          'sold_cost' => $output->price,
          'acquired_cost' => $input->price,
          'stock' => $stock,
          'resolved' => $resolved,
          'message' => $message
        );

        return $operation;
    }

    private function getTransactions()
    {
        $this->inputs = Transaction::where('type', 'IN')
            ->select('id', 'date', 'volumen', 'symbol', 'price', 'fee', 'currency')
            ->orderby('date', 'asc')
            ->get();

        $this->outputs = Transaction::where('type', 'OUT')
             ->select('id', 'date', 'volumen', 'symbol', 'price', 'fee', 'currency')
             ->orderby('date', 'asc')
             ->get();
    }
}
