<?php

namespace App\Repositories\Inventario\Methods;

use App\Repositories\Inventario\InventoryMethod;
use App\Models\Transaction;
use DB;

class Avg implements InventoryMethod
{
    private $operations = array();
    private $coins = array();
    private $outputs;
    private $inputs;

    /**
     * Obtiene las transacctiones y realiza el calculo
     */
    public function calculate()
    {
        $this->getTransactions();
        $this->inventarioCalculate($this->inputs, $this->outputs);
    }

    /**
     * Devuelve todas las operaciones calculadas
     *
     * @return array
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Devuelve todas las modenas que aun estan en el inventario
     *
     * @return array
     */
    public function getCoins()
    {
        return $this->coins;
    }

    /**
     * Calcula las operaciones en base a las entradas y salidas,
     * que deben realizarse en el inventario.
     *
     * @param array Illuminate\Database\Eloquent\Collection $inputs
     * @param array Illuminate\Database\Eloquent\Collection $outputs
     */
    private function inventarioCalculate($inputs, $outputs)
    {
        // Cicla por todas las salidas buscando que entradas las resolveran
        foreach ($outputs as $output) {

            // Obtiene las entradas que pueden resolver la salida
            $inputsToResolve = $inputs->where('volumen', '>', 0)
                             ->where('date', '<=', $output->date)
                             ->where('symbol', '=', $output->symbol);

            if ($inputsToResolve->isEmpty()) {

                // Calcula la operacion de salida como no resulta por ninguna
                // entrada.
                $operation = $this->calculateOperation($output, $output,
                                                       0, false);

                // Almacena la operacion calculada.
                array_push($this->operations, $operation);

                continue;
            }

            $averageCost = $this->calculateAverageCost($inputsToResolve);

            // Cicla por todas las entradas buscando que entradas resolveran
            // una salida.
            foreach ($inputsToResolve as $input) {
                $balance = $input->volumen - $output->volumen;

              // Obtiene la cantidad total en existencia
              $stock = $inputsToResolve->sum('volumen');

              // Calcula la operacion de salida como resulta por la entrada.
              $operation = $this->calculateOperation($input,
                           $output, true, $stock, $averageCost);

              // Si el balance es cero o mayor que cero significa que la
              // salida fue resuelta por completo por la entrada,de lo
              // contrario la salida aun debe continuar buscando otra
              // entrada que la resuelva por completo en un siguiente ciclo.
              if ($balance >= 0) {
                  $input->volumen -= $output->volumen;
                  $output->volumen = 0;
              }

                if ($balance < 0) {
                    $output->volumen -= $input->volumen;
                    $input->volumen = 0;
                }

              // Almacena la operacion calculada.
              array_push($this->operations, $operation);

              // Si el volumen de la salida es 0 significa que ya ha sido
              // resuelta por completo y termina el proceso de resolucion.
              if ($output->volumen == 0) {
                  break;
              }
            }

            $inputsToResolve = $inputsToResolve->map(function ($input, $key) use ($averageCost) {
                $input->price = $averageCost;
                $input->total = $input->volumen * $input->price;
                return $input;
            });

              // Si el volumen de la salida aun es mayor que cero significa que
              // la salida no ha podido ser resulta por ninguna entrada.
              if ($output->volumen > 0) {

                  // Obtiene la cantidad total en existencia.
                  $stock = $inputsToResolve->sum('volumen');

                  // Calcula la operacion de salida como no resulta por ninguna
                  // entrada.
                  $operation = $this->calculateOperation($output, $output,
                                                        $stock, false);

                  // Almacena la operacion calculada.
                  array_push($this->operations, $operation);

                  $output->volumen = 0;
              }
        }

        // Obtiene las entradas cuyo volumen sea mayor que cero.
        $inputs = $inputs->where('volumen', '>', 0);

        if (!$inputs->isEmpty()) {

            // Calcula el precio promedio de todas las monedas en inventario.
            $averageCost =  $this->calculateAverageCost($inputs);

            $inputs = $inputs->map(function ($input, $key) use ($averageCost) {
                $input->price = $averageCost;
                return $input;
            });

            // Las entradas cuyo volumenes sean mayores que cero las almacena
            // como monedas que aun estan en el inventario.
            foreach ($inputs as $input) {
                $coin = array(
                'currency' => $input->currency,
                'date_acquired' => $input->date,
                'coin' => $input->symbol,
                'volumen' => $input->volumen,
                'price' => $input->price,
                'fee' => $input->fee,
                'cost_basis' => ($input->volumen * $input->price) + $input->fee
              );

                array_push($this->coins, $coin);
            }
        }
    }

    /**
     * Calcula la operacion a realizar por una salida sobre una entrada.
     *
     * @param lluminate\Database\Eloquent\Collection $input
     * @param lluminate\Database\Eloquent\Collection $output
     * @param bool resolved
     *
     * @return array
     */
    private function calculateOperation($input, $output,
        $resolved = true, $stock, $averageCost = false)
    {
        $balance = $input->volumen - $output->volumen;
        $message = '';

        $averageCost = $averageCost ? $averageCost:$input->price;

        if ($balance <= 0) {
            $volume = $input->volumen;
        } elseif ($balance > 0) {
            $volume = $output->volumen;
        }

        $procceds = ($volume * $output->price) -$output->fee;

        if ($resolved) {
            $cost_basis = $volume * $averageCost;
        } else {
            $cost_basis = 0;
        }

        $gain_lost = $procceds - $cost_basis;
        $stock -= $volume;

        if (!$resolved) {
            $message =  sprintf("No matching buy for %f %s on %s "
                            ."Assuming zero-cost buy for %s %.2f on %s",
                              $output->volumen,
                              $output->symbol,
                              $output->datef,
                              $output->currency,
                              $gain_lost,
                              $output->datef
                            );
        }


        $operation = array(
        'input' => $input->id,
        'output' => $output->id,
        'volume' => $volume,
        'procceds' => $procceds,
        'cost_basis' => $cost_basis,
        'gain_lost' => $gain_lost,
        'sold_cost' => $output->price,
        'acquired_cost' => $averageCost,
        'stock' => $stock,
        'resolved' => $resolved,
        'message' => $message
      );

        return $operation;
    }

    /**
     * Calculate de average cost for a sell.
     *
     * @param Illuminate\Support\Collection $output
     * @param array Illuminate\Support\Collection $inputs
     */
    private function calculateAverageCost($inputs)
    {
        return  $inputs->sum('total') / $inputs->sum('volumen');
    }

    /*
     * Obtiene las transacciones filtradas por entradas y salidas.
     */
    private function getTransactions()
    {
        $this->inputs = Transaction::where('type', 'IN')
          ->select('id', 'date', 'volumen', 'symbol', 'price',
            'fee', 'currency', DB::raw('ABS(total) as total'))
          ->orderby('date', 'asc')
          ->get();

        $this->outputs = Transaction::where('type', 'OUT')
           ->select('id', 'date', 'volumen', 'symbol', 'price',
           'fee', 'currency', DB::raw('ABS(total) as total'))
           ->orderby('date', 'asc')
           ->get();
    }
}
