<?php

namespace App\Repositories\Inventario;

interface InventoryMethod
{
  public function calculate();
  public function getOperations();
  public function getCoins();
}
