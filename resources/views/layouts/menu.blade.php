<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs nav-custom">
          <router-link to="/trading" tag="li"><a>Trading</a></router-link>
          <router-link to="/income" tag="li"><a>Income</a></router-link>
          <router-link to="/spending" tag="li"><a>Spending</a></router-link>
          <router-link to="/opening" tag="li"><a>Opening Position</a></router-link>
          <router-link to="/reports" tag="li"><a>Reports & Exports</a></router-link>
        </ul>
    </div>
</div>
