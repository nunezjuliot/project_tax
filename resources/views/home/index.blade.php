@extends('layouts.admin')

@section('content-header')
  <h1>Taxes</h1>
@endsection

@section('content')
  @include('layouts.menu')
  <div class="row">
    <div class="col-md-12">
      <router-view></router-view>
    </div>
  </div>
@endsection
