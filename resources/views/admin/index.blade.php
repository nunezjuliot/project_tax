@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">

        <div class="col-md-2">
              <div class="list-group">
                <a href="#" class="list-group-item active">Eventos</a>
                <a href="#" class="list-group-item">Usuarios</a>
                <a href="#" class="list-group-item">Organizaciones</a>
                <a href="#" class="list-group-item">Roles</a>
                <a href="#" class="list-group-item">Apps</a>
              </div>
        </div>

        <div class="col-md-10">
            <div class="panel panel-default">
              <div class="panel-heading">Eventos</div>
              <div class="panel-body panel-table">
                <table class="table table-bordered table-hover table-data">
                  <thead>
                    <tr>
                      <th>Feha/Hora</th>
                      <th>Usuario</th>
                      <th class="col-md-7">Accion</th>
                      <th>Subject</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($events as $event)
                      <tr>
                        <td>{{$event->created_at}}</td>
                        <td>{{$event->causer->email}}</td>
                        <td>{{$event->description}}</td>
                        <td class="text-center">
                          <a>
                            <span class="glyphicon glyphicon-eye-open"></span>
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <div class="panel-footer">
                <div class="text-right">
                  {{ $events->links() }}
                </div>
              </div>
          </div>
     </div>
   </div>
  </div>
@endsection
