
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VueRouter from 'vue-router'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueRouter);

let trading  = require('./components/Trading.vue');
let income   = require('./components/Icome.vue');
let spending = require('./components/Spending.vue');
let opening  = require('./components/Opening.vue');
let reports  = require('./components/Reports/Index.vue');

const routes = [
  { path: '/', redirect: '/trading' },
  { path: '/trading', component: trading },
  { path: '/income', component: income },
  { path: '/spending', component: spending},
  { path: '/opening', component: opening},
  { path: '/reports', component: reports}
]

const router = new VueRouter({
  routes:routes,
  linkActiveClass: 'active'
})

Vue.http.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;

Vue.filter('currency', function(value,symbol){
  return accounting.formatMoney(value, {
          symbol: symbol,
          format: "%s %v",
        });
});

const app = new Vue({
  router
}).$mount('#app');
