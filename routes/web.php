<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::delete('trading/deleteall', 'TradingController@destroyAll');
Route::resource('trading', 'TradingController');

Route::delete('income/deleteall', 'IncomeController@destroyAll');
Route::resource('income', 'IncomeController');

Route::delete('spending/deleteall', 'SpendingController@destroyAll');
Route::resource('spending', 'SpendingController');

Route::delete('opening/deleteall', 'OpeningController@destroyAll');
Route::resource('opening', 'OpeningController');

Route::resource('coins', 'CoinController');
Route::resource('exchanges', 'ExchangeController');
Route::resource('currencies', 'CurrencyController');

Route::group(['prefix' => 'reports'], function () {
    Route::get('gains', 'InventarioController@gains');
    Route::get('gains/detail/{output}', 'InventarioController@sellDetail');
    Route::get('coins', 'InventarioController@coins');
    Route::get('coins/total', 'InventarioController@coinsTotal');
});

Auth::routes();
Route::get('admin/activity', 'AdminController@index')->name('activity');
