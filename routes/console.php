<?php

use Illuminate\Foundation\Inspiring;
use App\Models\Transaction;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

// Generate a export file of the transactions in excel format
Artisan::command('export {path} {types?}',
 function ($path, $types=['BUY','SELL']) {
     $colunms = [
      'Date',
      'Action',
      'Symbol',
      'Volume',
      'Currency',
      'Price',
      'Fee'
    ];

    //Create a CSV file.
    $report = fopen($path.'transactions.csv', 'w');

    //Write columns headers in the CSV file.
    fputcsv($report, $colunms);

    //Obtain the transactions and insert in the CSV file.
    Transaction::whereIn('action', $types)->chunk(100,
      function ($transactions) use ($report) {
          foreach ($transactions as $transaction) {
              fputcsv($report, [
            $transaction->date,
            $transaction->action,
            $transaction->symbol,
            $transaction->volumen,
            'USD',
            $transaction->price,
            $transaction->fee,
          ]);
          }
      });
 })->describe('Generate a export file of the transactions in excel format');
