<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Carbon\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Transaction::class, function (Faker\Generator $faker) {
    $action = array(
      'BUY',
      'SELL',
      'INCOME',
      'MINING',
      'GIFT/TIP',
      'SPEND',
      'STOLEN',
      'GIFT',
      'DONATION',
      'LOST',
      'OPENING'
    );

    return [
        'action' => $faker->randomElement($action),
        'currency' => 'VEF',
        'date' =>$faker
          ->dateTimeBetween($startDate = '-30 days', $endDate = '+1 week')
          ->getTimeStamp(),
        'exchange' => 'COINEX',
        'fee' => $faker->numberBetween($min = 0, $max = 30),
        'memo' => $faker->city(),
        'price' => $faker->numberBetween($min = 1, $max = 1000),
        'symbol' => 'BTC',
        'total' => function (array $transaction) {
            switch ($transaction['action']):
              case 'BUY':
                return -(($transaction['volumen'] * $transaction['price']) +
                           $transaction['fee']);
            break;

            case 'SELL':
                return (($transaction['volumen'] * $transaction['price']) -
                          $transaction['fee']);
            break;

            case 'OPENING':
                return ($transaction['volumen'] * $transaction['price']) +
                        $transaction['fee'];
            break;

            default:
                return $transaction['price'];
            break;

            endswitch;
        },
        'volumen' => number_format($faker->randomFloat($nbMaxDecimals = null, $min=0, $max=50), 8),
        'taxyear' => function (array $transaction) {
            return Carbon::createFromTimestamp($transaction['date'])->year;
        },
        'type' => function (array $transaction) {
            $inputs = array(
              'BUY',
              'INCOME',
              'MINING',
              'GIFT/TIP',
              'OPENING'
            );

            if (in_array($transaction['action'], $inputs)) {
                return 'IN';
            }

            return 'OUT';
        }
    ];
});

$factory->define(App\Models\Currency::class, function(Faker\Generator $faker){

      return[
          'abbreviation' => $faker->unique()->currencyCode()
      ];
});
