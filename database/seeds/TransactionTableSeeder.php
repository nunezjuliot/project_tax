<?php

use Illuminate\Database\Seeder;
use App\Models\Transaction;
use App\Repositories\Inventario\InventarioRepository;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaction::truncate();

        factory(App\Models\Transaction::class, 100)->create();

        $inventario = new InventarioRepository('Fifo');
        $inventario->calculate();
        $inventario->save();
    }
}
