<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        factory(App\User::class)->create([
            'name' => 'Mario David',
            'email' => 'mario@gmail.com',
            'password' =>  bcrypt('123456')
        ]);
    }
}
