<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            // Columns
            $table->increments('id');
            $table->string('action', 45);
            $table->string('currency', 45);
            $table->dateTime('date');
            $table->string('exchange', 45);
            $table->double('fee')->nullable()->default(0);
            $table->string('memo', 45)->nullable();
            $table->double('price');
            $table->string('symbol', 45);
            $table->double('total');
            $table->double('volumen');
            $table->smallInteger('taxyear')->unsigned();
            $table->enum('type', ['IN','OUT']);
            $table->timestamps();

            // Index
            $table->index('action');
            $table->index('currency');
            $table->index('exchange');
            $table->index('taxyear');
            $table->index('symbol');
            $table->index('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
