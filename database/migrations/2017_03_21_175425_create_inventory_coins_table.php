<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_coins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('currency', 45);
            $table->date('date_acquired');
            $table->string('coin', 45);
            $table->double('volumen');
            $table->double('price');
            $table->double('fee')->nullable()->default(0);
            $table->double('cost_basis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_coins');
    }
}
