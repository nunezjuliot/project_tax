<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('input');
            $table->integer('output');
            $table->double('volume');
            $table->double('procceds');
            $table->double('cost_basis');
            $table->double('gain_lost');
            $table->double('sold_cost');
            $table->double('acquired_cost');
            $table->double('stock');
            $table->boolean('resolved');
            $table->string('message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_operations');
    }
}
